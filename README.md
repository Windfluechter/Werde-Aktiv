# Werde-Aktiv

Werde-aktiv.de ist angedacht als Plattform dafür, Angebote für (ehrenamtliches) Engagement zu finden. Wenn man also irgendwas Sinnvolles machen möchte, aber noch nicht so recht weiß, wo man sich engagieren kann, soll werde-aktiv.de eine Möglichkeit bieten, interessierten Menschen entsprechende Angebote in ihrer Umgebung anzuzeigen. Dies können Sportvereine, Tafeln, NGOs, Umweltverbände oder ähnliches sein.

Der Service soll

* datenschutzfreundlich und Open Source sein und kein Tracking verwenden
* Als Karte sollte OpenStreetMap zum Einsatz kommen
* Interessierte sollen ihren Wohnort eingeben können (Ortsname/PLZ) oder in die Karte hinzoomen
* Es sollten Filtermöglichkeiten nach Art des Engagements möglich sein, z.B. Sport, Soziales, Datenschutz, ...
* Normale Nutzende sollten eigentlich keinen Account benötigen. Verwaltende oder Vereine, NGOs, etc. sollten einen Account bekommen können, um die eigenen Daten verwalten zu können und vielleicht einen kurzen Steckbrief erstellen zu können
* Eventuell braucht es einen Scraper, um die Datenbank mit Vereinen & Co zu befüllen. Es sollte auch eine Opt-Out-Möglichkeit geben (robots.txt oder ähnliches?)
* ...?

## Weitere Information
Es gibt eine Mailing Liste auf http://lists.bluespice.org/mailman/postorius/lists/dev.lists.werde-aktiv.de/members/member/ - dort werden die ersten Schritte besprochen und abgestimmt. 